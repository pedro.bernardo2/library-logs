package com.avenudecode.library.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.avenudecode.library.entity.Book;
import com.avenudecode.library.repository.LibraryRepository;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(propagation = Propagation.SUPPORTS)
@RequiredArgsConstructor
public class LibraryService {

	@Autowired
	private LibraryRepository libraryRepository;

	Logger logger = LoggerFactory.getLogger(LibraryService.class);

	public List<Book> getAll() {

		logger.info("Return from repository to all the books: {}", libraryRepository.findAll());

		return libraryRepository.findAll();
	}

	public Book save(Book book) {

		logger.info("Return from repository to the saved Book: {}", libraryRepository.save(book));

		return libraryRepository.save(book);
	}

	public Book updateBook(Book book) {
		Optional<Book> updateBook = libraryRepository.findById(book.getId());

		if (updateBook.isPresent()) {

			logger.info("Return from repository to the Book with id: {} {}", book.getId(), updateBook);

			updateBook.get().setAuthor(book.getAuthor());
			updateBook.get().setTitle(book.getTitle());
			updateBook.get().setRented(book.getRented());

		} else {

			logger.error("Can't find Book with id: {}", book.getId());

			throw new RuntimeException(String.format("Não foi possivel encontrar o livro"));
		}

		logger.info("Return from repository to the updated Book: {}", libraryRepository.save(updateBook.get()));

		return libraryRepository.save(updateBook.get());

	}

	public void deleteBook(String id) {
		Optional<Book> deleteBook = libraryRepository.findById(id);

		if (!deleteBook.isPresent()) {

			logger.error("Can't find Book with id: {}", id);

			throw new RuntimeException(String.format("Não foi possivel encontrar o livro"));

		} else {

			logger.info("Return from repository to the Book with id: {} {}", id, deleteBook.get());

			libraryRepository.delete(deleteBook.get());
		}

	}

	public Book getBookById(String id) {

		Optional<Book> bookFound = libraryRepository.findById(id);

		if (!bookFound.isPresent()) {

			logger.error("Can't find Book with id: {}", id);

			throw new RuntimeException(String.format("Não foi possivel encontrar o livro"));

		} else {

			logger.info("Return from repository to the Book with id: {} {}", id, bookFound.get());

		}

		return bookFound.get();
	}

	public List<Book> getBooksNotRented() {

		logger.info("Return from repository to all the books that are not rented: {}",
				libraryRepository.getBooksNotRented());

		return libraryRepository.getBooksNotRented();
	}

}
