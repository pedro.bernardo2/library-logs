package com.avenudecode.library.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.avenudecode.library.entity.Book;
import com.avenudecode.library.service.LibraryService;

@RestController
@RequestMapping("/library")
public class LibraryController {
	
	@Autowired
	private LibraryService libraryService;
	
	Logger logger = LoggerFactory.getLogger(LibraryController.class);
	
	@GetMapping
	public List<Book> getAll() {
		
        logger.info("Requesting all the books");

		return libraryService.getAll();
	} 
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public Book saveBook(@RequestBody Book book) {

        logger.info("Requesting to save a New Book");
        
		return libraryService.save(book);				
	}
	
	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public Book updateBook(@RequestBody Book book) {
		
		logger.info("Requesting to update a Book");
		
		return libraryService.updateBook(book);
	}
	
	@DeleteMapping("/{id}")
	public void deleteBook(@PathVariable(value = "id") String id) {
		
		logger.info("Requesting to delete the Book with Id: {}", id);
		
		libraryService.deleteBook(id);
	}
	
	@GetMapping("/{id}")
	public Book getBookById(@PathVariable(value = "id") String id) {
		
		logger.info("Requesting the Book with Id: {}", id);
		
		return libraryService.getBookById(id);
	
	}
	
	@GetMapping("/booksnotrented")
	public List<Book> getBooksNotRented() {
		
		logger.info("Requesting all the books that are Not Rented");
		
		return libraryService.getBooksNotRented();
	}
	
}